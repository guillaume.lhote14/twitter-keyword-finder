# Twitter Keyword Finder

Script to find a keyword in your Twitter follower screen names

## Prepare

```sh
cp secrets.example.py secrets.py
```

## Installation

First, update your API keys in the secrets.py file. To get API keys go to https://apps.twitter.com/

Then you need to install the requirements by running:

```sh
pip install -r requirements.txt
```

## Usage

```sh
python poc.py <screen_name> <keyword>
```

After completion a file screen_name_keyword.txt with the results is created.

## Credits

This script has been made with ❤ ️by @fs0c131y
